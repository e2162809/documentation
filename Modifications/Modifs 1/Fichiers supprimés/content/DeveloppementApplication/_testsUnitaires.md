---
title: "Les tests unitaires"
weight: 6
date: 2022-01-11T14:19:27Z
draft: true
---

Les tests unitaires consistent à tester individuellement les composants de l'application. On pourra ainsi valider la qualité du code et les performances d'un module.

## C'est quoi un test unitaire?
En programmation informatique, le **test unitaire** (ou «  **T.U.**  », ou «  **U.T.**  » en anglais) ou **test de composants** est une procédure permettant de vérifier le bon fonctionnement d'une partie précise d'un logiciel (extrait de code) ou d'une portion d'un programme (appelée « unité » ou « module »).

Les tests unitaires permettent de s'assurer que le module fonctionne selon les spécifications. Par les tests unitaires on vérifie que chaque fonctionnalité produit bien les résultats escomptés. La phase de tests unitaires survient à l'écriture du code, avant les tests d'intégration.

Un test unitaire vérifie essentiellement les sorties obtenues (résultats) à partir des entrées fournies (données). Pour chaque fonctionnalité, il faut écrire plusieurs tests qui vérifient les résultats avec:

- des données typiques;
- des données limites;
- des données invalides (vérification des exceptions générées).  

## Pourquoi faire les tests unitaires?
En tant que programmeur, vous devriez intégrer les tests unitaires lors de la conception d'un programme pour les raisons suivantes :

- Améliorer la qualité de votre code en détectant les anomalies fonctionnelles dans la phase de développement;
- Améliorer la lisibilité du code ce qui facilitera l'ajout des nouveaux composants ou les modifications du ou d'une partie du code;
- Enrichir la documentation du projet et démontre le bon fonctionnement du code aux clients;
- Gagner du temps lors de l'étape de développement en minimisant les bogues.
- Tester les différentes parties du projet d'une manière indépendante.

## Les bonnes pratiques des tests unitaires

**Un test doit respecter certaines règles pour qu'il soit efficace. On retrouve parmi ces règles:**

- Un test doit être unitaire et concerne une seule partie du code;
- Un test doit être simple, normé, commenté, facile à lire et à comprendre.
- Un test doit être rapide à exécuter (la suite de tests doit pouvoir être exécutée souvent).
- Il est préférable d'écrire les tests avant l'étape de développement;
- Il est préférable de tester en priorité les parties critiques du projet;
- Il faut utiliser une appellation significatif permettant de savoir ce que va faire le test à partir de son nom.  

**Les bonnes pratiques en matière de tests logiciels unitaires sont:**

- Les tests unitaires devraient être indépendants. Les tests unitaires ne devraient pas être affectés en cas d'amélioration ou de modification des exigences.
- Ne testez qu'un seul extrait de code à la fois.
- Suivez un plan clair et précis. Cela peut sembler accessoire par rapport à d'autres pratiques, mais ce n'est pas le cas. Soyez cohérent lorsque vous nommez vos tests unitaires.
- Tout changement mis en œuvre doit réussir les tests. S'il y a un changement dans le code d'un des modules, assurez-vous qu'il y a des tests unitaires pertinents à ce module et que ce module réussit les tests avant de mettre en œuvre complètement le changement.
- Corrigez tous les bugs qui ont pu être identifiés pendant les tests avant de continuer. Assurez-vous de bien comprendre ce point avant de passer à l'étape suivante du cycle de vie du développement logiciel.
- Effectuer des tests régulièrement pendant la programmation, développe un automatisme chez le programmeur.

## C'est quoi une plateforme de test?

En informatique, une plateforme désigne un matériel ou un logiciel qui héberge une application ou un service. Une plateforme de test est un framework qui permet l'écriture des tests unitaires grâce à une librairie de test javascript.

Jest , la plateforme de test développée par Facebook en 2014, devient de plus en plus populaire permet de tester les applications React. Jest est rapide, facile à utiliser et propose de nombreuses fonctionnalités (telles que les tests d'instantanés et la couverture des tests).

## Fonctionnement d'une plateforme de tests Jest

La plateforme Jest est conçue pour fonctionner aussi bien sur du JavaScript côté navigateur (frontend) que côté serveur (backend), Jest a su convaincre par sa rapidité d'exécution des tests, son API complète et sa facilité d'installation. On peut choisir des fonctionnalités spécifiques de Jest et les utiliser en tant que packages autonomes.

Par défaut, Jest (dans notre cas) cherche dans tous les sous-dossiers (à part node\_modules et git , notamment) à la recherche de fichiers se terminant par spec.js ou test.js , précédé d'un trait d'union (  -  ) ou d'un point (  .  ).

À la création d'un fichier test, on lui attribue une extension selon la convention de l'équipe de développement .spec.js ou .test.js

Jest peut être intégrer dans un script ou exécuter en ligne de commande

## Installation et utilisation de jest
Installer le package de jest: **npm install jest --save-dev**

Configurer votre eslint pourqu'il prenne en charge jest: **npm install --save-dev eslint eslint-plugin-jest**

Modifier le fichier .eslintrc.js:
```js
module.exports = {
	env: {
		browser: true,  
		es2020: true,  
		jest: true,  
	},
	extends: [
		'airbnb-base',
		'plugin:jest/recommended',
	],
	parserOptions: {
		ecmaVersion: 12,
	},
	rules: {
		'no-console': 'OFF',
	},
};
```

Ajouter la section suivante dans package.json
```js
{
	"scripts": {
	"test": "jest"
	}
}
```

Pour lancer le test: **npm run test ou npm test**

Pour exécuter jest en ligne de commande, il faut que le package soit installé globalement avec la commande: **npm install jest –global**  
On exécute ensuite Jest ainsi:  

**jest exemple.test.js --config=config.json**

Jest exécute le fichier exemple.test.js en utilisant le fichier config.json comme fichier de configuration.  

## Construction d'un test
On veut, par exemple, vérifier le fonctionnement du fichier *somme.js* :  
```js
function somme(a, b) {
	return a + b;
}

module.exports = somme;
```
On ajoute un fichier *somme.test.js*. Ce fichier peut contenir autant de tests que nécessaire pour vérifier le fonctionnement du fichier *somme.js*.  

Un test est construit en utilisant la fonction test(). Cette fonction prend 2 arguments: une chaîne représentant le nom du test et une fonction fléchée contenant le code du test.  
```js 
test(<nom du test>, () => {
// Préparation du test...

// Opération à tester...

// Une ou plusieurs vérifications sous la forme "d'attentes"...
	expect(<résultat obtenu>).<vérification>;
});  
```
La vérification est effectuée par l'appel d'une fonction de vérification de Jest, appelée "matcher" ou "vérificateur". Dans le fichier *somme.test.js*, le test vérifie l'égalité de deux valeurs à l'aide du vérificateur toEqual().  
```js
const somme= require('./somme');

test('additionner 1 + 2 égale 3', () => {
	expect(sum(1, 2)).toEqual(3);
});
```
Le test est exécuté en tapant  **npm test** au terminal ou bien simplement en sauvegardant le test si l'extension Jest est installée dans votre environnement de développement.  

### **Différents vérificateurs (matchers)**
**Comparaison de nombres**
```js
test('2 plus 2', () => {
	const valeur = 2 + 2;

	expect(valeur).toEqual(4);
		expect(valeur).toBeGreaterThan(3);
	expect(valeur).toBeGreaterThanOrEqual(4);
	expect(valeur).toBeLessThan(5);
	expect(valeur).toBeLessThanOrEqual(4);
});
```
**Inverse d'un vérificateur**  
```js
test('2 plus 2', () => {
	const valeur = 2 + 2;

	expect(valeur).not.toEqual(5);
		expect(valeur).not.toBeGreaterThan(6);
	expect(valeur).toBeGreaterThanOrEqual(4);
	expect(valeur).toBeLessThan(5);
	expect(valeur).toBeLessThanOrEqual(4);
});
```
**Vérificateurs de vérité (truthiness), null et undefined**  

**toBeTruthy()** : vérifie si true tel que le fait l'instruction if  
**toBeFalsy()** : vérifie si false tel que le fait l'instruction if  
**toBeNull()** : vérifie si null  
**toBeUndefined()** : vérifie si undefined  
**toBeDefined()** : vérifie l'inverse of toBeUndefined  

**Exemples :**
```js
test('null', () => {
	const n = null;

	expect(n).toBeNull();
	expect(n).toBeDefined();
	expect(n).not.toBeUndefined();
	expect(n).not.toBeTruthy();
	expect(n).toBeFalsy();
});

test('zero', () => {
	const z = 0;

	expect(z).not.toBeNull();
	expect(z).toBeDefined();
	expect(z).not.toBeUndefined();
	expect(z).not.toBeTruthy();
	expect(z).toBeFalsy();
});
```
**Vérificateurs de chaînes de caractères**   
Vous pouvez vérifier les chaînes de caractères par rapport aux expressions régulières avec toMatch :
```js
test("il n'y a pas de I dans bateau", () => {
  expect('bateau').not.toMatch(/I/);
});

test('mais il y a de l\'eau dans le bateau' () => {
  expect('bateau').toMatch(/eau/);
});
```
**Vérification d'éléments dans les tableaux et collections**
```js
test('jours contient jeudi', () => {
	const jours = [
	'lundi',
	'jeudi',
	'vendredi',
	];

	expect(jours).toContain('jeudi');
	expect(new Set(jours)).toContain('jeudi');
});
```

**Test d'une exception**  
On peut vérifier qu'une fonction lance une exception dans certaines conditions.  
```js
function susceptible() {
	throw new Error('Exception à tout coup');
}

function positive(idee) {
	if (idee < 0) {
	throw new Error('Je n\'accepte pas les idées négatives');
	}

	return ('Vive la vie!');
}
```
La syntaxe du test est un peu différente. L'argument de la fonction expect(), c'es-à-dire l'appel de fonction qui génère une exception, doit être englobé dans une fonction fléchée:  
```js
test('susceptible lance toujours des exceptions', () => {
	// Assert
	expect(() => {
	ex.susceptible();
	}).toThrow(Error);

	expect(() => {
	ex.susceptible();
	}).toThrow('à tout coup');

	expect(() => {
	ex.susceptible();
	}).toThrow(new Error('Exception à tout coup'));

	expect(() => {
	ex.susceptible();
	}).toThrow(new Error('à tout coup')); // Ce test échoue
});

test('positive() n\'aime pas les idées négative', () => {
	// Assert
	expect(() => {
	ex.positive(-2);
	}).toThrow(Error);

	expect(() => {
	ex.positive(-2);
	}).toThrow('idées négatives');
});

test('positive() n\'aime pas les idées négative, construction plus simple', () => {
	// Arrange
	function positiveMoins1000() {
	ex.positive(-1000);
	}

	// Assert
	expect(positiveMoins1000).toThrow(Error);
	expect(positiveMoins1000).toThrow('idées négatives');
});
```
**Test de code asynchrone**  
Pour tester du code asynchrone, la fonction fléchée du test doit être déclarée asynchrone et attendre le résultat du traitement asynchrone.  
```js
test('Exemple de test qui vérifie que la fonction asynchrone retourne "allo!" ', async () => {
  const donnee = await fetchDonnee();
  expect(donnee).toEqual('allo!');
});
```
On peut tester aussi que le code asynchrone génère une exception. Il s'agit d'englober le code dans un try...catch. 
```js
test('la fonction génère une exception avec un message contenant "Erreur"', async () => {
  expect.assertions(1);
  try {
    await fetchDonnees();
  } catch (e) {
    expect(e).toMatch('Erreur');
  }
});
```
