---
title: "Premier pas"
weight: 3
date: 2022-01-11T14:19:27Z
draft: false
---

### Créer un dépôt local
Un dépôt Git est une sorte de base de données de votre projet de code. Il vous permet d'enregistrer les versions de votre code et d'y accéder au besoin. Il y a deux façons de créer un dépôt local:
*  Initialiser un nouveau dépôt localement (sur votre ordinateur);
*  Copier (cloner) un dépôt distant vers votre ordinateur. 

**Initialiser un nouveau dépôt local: git init**  
Pour créer un dépôt, utilisez la commande *git init* à partir du dossier racine de votre projet. Lorsque vous exécutez cette commande, un sous-dossier .git est créé dans votre dossier de projet. De même, une branche principale est créée.  

Si votre dossier de projet contient déjà le code source de votre projet, il faut maintenant enregistrer l'état du projet dans le dépôt Git situé dans le dossier *.git*. L'état du projet est un "snapshot" de tous les fichiers de votre dossier de projet. Deux commandes sont nécessaires:  

```posh
>git init
>git add .
>git commit -m "Commit initial"
```

*git add* indique quels fichiers seront enregistrés dans la base de données Git lors du commit. Cette opération s'appelle l'indexation ou bien le "staging". Dans le cas-ci, " . " désigne tous les fichiers.  
*git commit* enregistre les fichiers indexés dans la base de données du dépôt. Chaque dépôt doit être documenté à l'aide d'un message texte.  Les différents commits sont identifiés par un code de hachage fait sur tous les fichiers du snapshot.  

### Créer un dépôt distant  
Habituellement, vous allez travailler en équipe sur un projet. Dans ce cas, vous aurez besoin d'un dépôt distant pour synchroniser votre travail avec celui de vos équipiers. Un membre de l'équipe crée le dépôt sur un serveur distant (GitLab, GitHub, BitBucket, ...) et les autres clonent ce dépôt sur leur ordinateur.  

Vous utiliserez la même stratégie pour un projet personnel que vous voulez utiliser à partir de plusieurs ordinateurs. Vous aurez un dépôt distant sur un serveur Git et vous clonerez ce dépôt sur les ordinateurs où vous voulez travailler.  

Encore une fois, il existe deux façons de créer un dépôt dépôt distant:
*  Initialiser un nouveau dépôt sur un serveur Git (GitLab ou autre);
*  "Pousser" un dépot local vers un serveur Git.  

La première option est la plus simple. Puisque nous avons déjà créé un dépôt local, nous allons faire la deuxième technique.

**Créer un dépôt distant**  
Sur le site GitLab, choisissez l'option *Create new project* dans le menu *Projets*. Choisissez ensuite l'option *Create blank project*.  

*Nom du projet*: donnez un nom à votre projet  
*URL du projet*: indiquer votre nom d'utilisateur, qui correspond à votre espace de nom sur GitLab 
*Visibility level*: choisissez *Public*  

---
![git projet 2](http://ml.cm9.ca/cours3d1/images/gitProjet2.png)  

---

**Configurer un dépôt local pour le synchroniser à un dépôt distant existant**  
Lorsque le dépôt distant est créé, il faut configurer notre dépôt local pour qu'il puisse interagir avec le dépôt sur GitLab.  

```posh
>cd dossier_de_projet
>git remote add origin https://gitlab.com/Marc64/monprojet2.git
>git push -u origin main
```
La commande *git remote* ajoute un dépôt distant dans les références de dépôts (les "remotes") et lui donne l'alias *origin*. À partir de maintenant, *origin* indique le dépot sur GtiLab.  
La commande *git push* pousse les modifications du dépôt local (tous les commits qui ne sont pas déjà enregistrés sur le dépôt distant) sur la branche *main* du dépôt distant spécifié par *origin*. Le paramètre *-u* indique de conserver les références du dépôt distant et de la branche utilisée. C'est ce que nous appelons le *upstream* ou "la branche en amont". Ainsi au prochain *push* sans paramètre de la branche *main* du dépôt local, Git poussera la branche *main* sur la branche *main* du dépôt sur Gitlab.  

**Cloner un dépôt distant existant : git clone**  
Lorsque le dépôt distant est créé, les autres membres de l'équipe exécutent la commande *git clone* sur un dossier vide de leur poste de travail pour obtenir une copie du dépôt distant.  

Votre équipe dispose maintenant d'un dépôt décentralisé. Si vous êtes 3 dans votre équipe et que tous les dépôts sont synchronisés, vous disposez maintenant de 4 exemplaires de votre dépôt d'équipe. 

**Si nous récapitulons, nous avons fait les étapes suivantes:**  
1.  Créer un dépôt local;
2.  Créer un dépôt distant;
3.  Configurer le dépôt local pour qu'il interagisse avec le dépôt distant;
4.  Pousser les modifications du dépôt local vers le dépôt distant (synchroniser le dépôt distant à partir du dépôt local);
5.  *Pour les autres membres de l'équipe*: cloner le dépôt distant sur leur poste de travail. En plus de faire une copie du dépôt distant, le clonage fait automatiquement l'étape 3 de configuration.  

Un autre sratégie est de commencer par la création du dépôt distant. Dans ce cas, il est important de créer le dépôt distant avec un fichier README de façon à pouvoir cloner le dépôt avant même le premier push:

---
![git projet 1](http://ml.cm9.ca/cours3d1/images/gitProjet1.png)  

---

**Les étapes pour cette stratégie sont les suivantes**:  
1.  Créer un dépôt distant avec un fichier README;
2.  Cloner le dépôt distant sur tous les postes de travail des membres de l'équipe;

